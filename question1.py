# Q1
import re
import shutil

ptnSpace=re.compile("\s+") # \s+ catches any space or tab char 1 or more tabs
ptn=re.compile("#.*")
hostsFH=open("hosts.real", "r")
# newfile putput
FH=open("hostsNew.txt", "w")
ipFH=open("ipAddress.txt", "w")

# MAC pattern identifier
macptn=re.compile("[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}")

print("\nThe MAC addresses are:\n")
for line in hostsFH:
    # Remove #
    if re.search("^#", line):
        continue
    if re.search('#', line):
        line=ptn.sub("",line)
    # Remove Space
    data=ptnSpace.split(line)
    FH.write(" ".join(data)+"\n")
    # Write IP addresses to a new file
    ipFH.write(data[0]+"\n")
    # Print Mac Addresses
    mac=macptn.search(line)
    if mac:
        print(mac.group())

# Close files
hostsFH.close()
FH.close()
ipFH.close()
shutil.move('hostsNew.txt', 'hosts.real')
