import re
import time

#Find and extract all trades

opraFH=open("opra_example_regression.log")
RP=TT=TV=TP=""

ptnSpace=re.compile("\s+") # \s+ catches any space or tab char 1 or more tabs
ptn=re.compile("Regression:")
RPptn=re.compile("Record Publish")
TTptn=re.compile("Type: Trade")
TVptn=re.compile("wTradeVolume")
TPptn=re.compile("wTradePrice")

count = 0

for line in opraFH:
    #Remove Regression
    if re.search("^Regression:&",line):
        continue
    if re.search('Regression:',line):
        line=ptn.sub("",line)
    #Remove lines
    data=ptnSpace.split(line)
    #print(" ".join(data))
    if RPptn.search(line):
        RP=line
    if TTptn.search(line):
        TT=line
    if RP and TT:
        if TVptn.search(line):
            TV=line
        if TPptn.search(line):
            TP=line
    if RP and TT and TV and TP:
        count += 1
        print(RP.rstrip("\r\n")+TT.rstrip("\r\n"))
        print(TV+TP.rstrip("\r\n"))
        RP=TT=TV=TP=""

print("\nTotal number of trades:",count)

        #print all and reset variables, loop round again. There
        #should only be 95 trades
